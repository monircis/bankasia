<?php include_once 'db_connection.php'; error_reporting(0); 
session_start();
if($_SESSION['logged_in'] != true){
header("location:login.php");
}

    $message ="";
    if(isset($_POST['add_member'])){
        $member_name = $_POST['member_name'];
        $chairmen = $_POST['chairmen'];
        $key_exe = $_POST['key_exe'];        
        $member_designation = $_POST['member_designation'];
        $member_message = $_POST['member_message'];
        $target = "memberphoto/";
        
          $tmp_file = $_FILES['member_image']['tmp_name'];
        $ext = pathinfo($_FILES["member_image"]["name"], PATHINFO_EXTENSION);
        $rand = md5(uniqid().rand());
        $post_image = $rand.".".$ext;
      
        
        
        if(move_uploaded_file($tmp_file,"memberphoto/".$post_image)){
        $sql="INSERT INTO member (member_name,member_designation,member_message,member_image,chairmen,key_exe) VALUES ('$member_name','$member_designation','$member_message','$post_image','$chairmen','$key_exe')"; 
        mysql_query($sql);
        
        $message='<div class="alert alert-success">
        <a class="close" href="#" data-dismiss="alert">
        <i class="fa fa-times-circle"></i>
        </a>
        You  have  Successfully Inserted.
        </div>';
       }
      
 }

 ?>
<!DOCTYPE html>
<html>
<head>
    <title>BankAsia</title>
    <link href="css/application.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
</head>
<body>
 
<?php 
 


include_once 'inc/sidebar.php'; 
include_once 'inc/header.php'; 
?>
<!-- This is the white navigation bar seen on the top. A bit enhanced BS navbar. See .page-controls in _base.scss. -->


 

<div class="content-wrap">
    <!-- main page content. the place to put widgets in. usually consists of .row > .col-md-* > .widget.  -->
    <main id="content" class="content" role="main">
        <ol class="breadcrumb">
            <li>YOU ARE HERE</li>
            <li class="active">Member Page</li>
        </ol>
       
       <span class="clearfix"></span>
                        <?php  echo $message ; ?>
         
        <div class="row">
            <div class="col-md-12">
                <section class="widget">
                    <header>
                        <h5>
                            
                            Member
                        </h5>
                        <div class="widget-controls">
                            
                            <a href="#"><i class="fa fa-refresh"></i></a>
                           
                        </div>
                       
                    </header>
                    <div class="widget-body">
                    
                        <form class="form-horizontal" method="POST" action=" " role="form" enctype="multipart/form-data">
                            <fieldset>
                                <legend> &nbsp;</legend>
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label" for="member_name">
                                        Member Name
                                        
                                    </label>
                                    <div class="col-sm-9">
                                       <input type="text" name="member_name" class="form-control" required="" />
                                        
                                    </div>
                                </div>
                             
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="member_designation">
                                        Member Designation
                                       
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" name="member_designation" class="form-control" required="" />
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fileupload1">
                                        Selcet  Member Image
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput">
                                                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                <span class="fileinput-filename"></span>
                                            </div>
                                            <span class="input-group-addon btn btn-default btn-file">
                                                <span class="fileinput-new">Select Image File</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input id="fileupload1" name="member_image" type="file">
                                            </span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                        <span class="help-block">Image  size  270px  width  height  270px.</span>
                                 
                                  
                                    </div>
                                </div>
                                   <div class="form-group">
                                    <label class="col-sm-3 control-label" for="member_message">
                                        Member message
                                        <small>(optional)</small>
                                       
                                    </label>
                                    <div class="col-sm-9">
                                        <textarea rows="8" class="form-control" id="wysiwyg" name="member_message" ></textarea>
                                      
                                    </div>
                                </div>
                                 
                                   <div class="form-group">
                                    <label class="col-sm-3 control-label" for="key_exe">
                                        Key Executive
                                       
                                    </label>
                                    <div class="col-sm-9">
                                          <div class="checkbox checkbox-danger">
                                            <input id="checkbox9" type="checkbox" name="key_exe" value="yes"  />
                                            <label for="checkbox9">
                                                Yes
                                            </label>
                                        </div>
                                                                     
                                    </div>
                                </div>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label" for="member_designation">
                                        Chairmen
                                       
                                    </label>
                                    <div class="col-sm-9">
                                          <div class="checkbox checkbox-danger">
                                            <input id="checkbox6" type="checkbox" name="chairmen" value="yes"  />
                                            <label for="checkbox6">
                                                Yes
                                            </label>
                                        </div>
                                     
                                    <div class="btn-toolbar">
                                            <button type="submit" name="add_member" class="btn btn-md btn-danger pull-right">Save</button>
                                        </div>
                                    </div>
                                </div>
                                
                                 
                            </fieldset>
                        </form>
                    </div>
                </section>
            </div>
             
        </div>
        <div class="row">
          <?php 
            
            $sql="SELECT * FROM member";
            $result= mysql_query($sql);
            while($row=mysql_fetch_assoc($result)){
            ?>
                       
                       
        <div class="col-md-4" style="overflow: hidden !important;display: inline-block;">
                <section class="widget" style="overflow: hidden !important;height: 320px;">
                    <header>
                        <h5>
                            <?php  echo $row['member_name']; ?>
                        </h5>
                         <p class="text-justify"><?php  echo $row['member_designation'];?></p>
                         
                        <div class="widget-controls">
                         <a href="edit-member.php?member_id=<?php echo $row['member_id'];  ?>" ><i class="glyphicon   glyphicon-edit"></i></a>
                 
                              <a href="del-member.php?member_id=<?php echo $row['member_id'];  ?>" ><i class="glyphicon   glyphicon-trash"></i></a>
                        </div>
                    </header>
                    <div class="widget-body">
                    <img src="memberphoto/<?php echo $row['member_image']; ;  ?>" style=" overflow: hidden;" class="img-responsive img-thumbnail pull-left"  />
                      
                    </div>
                </section>
            </div>
        
  <?php }?>
        
             
        </div>
         
         
         
         
    </main>
</div>
<!-- The Loader. Is shown when pjax happens -->
<div class="loader-wrap hiding hide">
    <i class="fa fa-circle-o-notch fa-spin-fast"></i>
</div>

<!-- common libraries. required for every page-->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/jquery-pjax/jquery.pjax.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/transition.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/collapse.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/button.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/alert.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/widgster/widgster.js"></script>
<script src="vendor/pace.js/pace.min.js"></script>
<script src="vendor/jquery-touchswipe/jquery.touchSwipe.js"></script>

<!-- common app js -->
<script src="js/settings.js"></script>
<script src="js/app.js"></script>

<!-- page specific libs -->
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/modal.js"></script>
<script src="vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="vendor/jquery-autosize/jquery.autosize.min.js"></script>
<script src="vendor/bootstrap3-wysihtml5/lib/js/wysihtml5-0.3.0.min.js"></script>
<script src="vendor/bootstrap3-wysihtml5/src/bootstrap3-wysihtml5.js"></script>
<script src="vendor/select2/select2.min.js"></script>
<script src="vendor/switchery/dist/switchery.min.js"></script>
<script src="vendor/moment/min/moment.min.js"></script>
<script src="vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script src="vendor/jasny-bootstrap/js/inputmask.js"></script>
<script src="vendor/jasny-bootstrap/js/fileinput.js"></script>
<script src="vendor/holderjs/holder.js"></script>
<script src="vendor/dropzone/dist/min/dropzone.min.js"></script>
<script src="vendor/markdown/lib/markdown.js"></script>
<script src="vendor/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script src="vendor/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js"></script>

<!-- page specific js -->
<script src="js/form-elements.js"></script>
</body>
</html>