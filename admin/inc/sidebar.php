<nav id="sidebar" class="sidebar" role="navigation">
    <!-- need this .js class to initiate slimscroll -->
    <div class="js-sidebar-content">
        <header class="logo hidden-xs">
            <a href="http://bankasia.silkyit.com/" target="_blank">BankAsia</a>
        </header>
       
        <ul class="sidebar-nav">
          <li>
                
                <a class="collapsed" href="#sidebar-Services" data-toggle="collapse" data-parent="#sidebar">
                    <span class="icon">
                        <i class="fa fa-desktop"></i>
                    </span>
                    Services
                    <i class="toggle fa fa-angle-down"></i>
                </a>
                <ul id="sidebar-Services" class="collapse">
                    <li><a href="service-input.php">Add  New Service</a></li>
                  
                </ul>
            </li>
               <li>
                
                <a class="collapsed" href="#sidebar-News" data-toggle="collapse" data-parent="#sidebar">
                    <span class="icon">
                        <i class="fa fa-desktop"></i>
                    </span>
                    News 
                    <i class="toggle fa fa-angle-down"></i>
                </a>
                <ul id="sidebar-News" class="collapse">
                    <li><a href="news.php">Add  New News</a></li>
                  
                </ul>
            </li>
               <li>
                
                <a class="collapsed" href="#sidebar-Research" data-toggle="collapse" data-parent="#sidebar">
                    <span class="icon">
                        <i class="fa fa-desktop"></i>
                    </span>
                    Research
                    <i class="toggle fa fa-angle-down"></i>
                </a>
                <ul id="sidebar-Research" class="collapse">
                    <li><a href="research-info.php">Add  New Research</a></li>
                  
                </ul>
            </li>
            <li>
                <!-- an example of nested submenu. basic bootstrap collapse component -->
                <a class="collapsed" href="#sidebar-dashboard" data-toggle="collapse" data-parent="#sidebar">
                    <span class="icon">
                        <i class="fa fa-desktop"></i>
                    </span>
                     Useful Links
                    <i class="toggle fa fa-angle-down"></i>
                </a>
                <ul id="sidebar-dashboard" class="collapse">
                    <li><a href="add-link.php">Add  New Link</a></li>
                </ul>
            </li>
              <li>
                
                <a class="collapsed" href="#sidebar-contact" data-toggle="collapse" data-parent="#sidebar">
                    <span class="icon">
                        <i class="fa fa-desktop"></i>
                    </span>
                    Branch Informations
                    <i class="toggle fa fa-angle-down"></i>
                </a>
                <ul id="sidebar-contact" class="collapse">
                    <li><a href="add-branch.php">Add  New Branch </a></li>
                     
                </ul>
            </li>
             <li>
                
                <a class="collapsed" href="career.php">
                    <span class="icon">
                        <i class="fa fa-desktop"></i>
                    </span>
                    Career
                    <i class="toggle fa fa-angle-down"></i>
                </a>
                
            </li>
               <li>
                
                <a class="collapsed" href="#sidebar-team" data-toggle="collapse" data-parent="#sidebar">
                    <span class="icon">
                        <i class="fa fa-desktop"></i>
                    </span>
                    Our Team
                    <i class="toggle fa fa-angle-down"></i>
                </a>
                <ul id="sidebar-team" class="collapse">
                    <li><a href="member-input.php">Add  New Member</a></li>
                    <li><a href="all-member.php">All Team Member</a></li>
                     <li><a href="team-overview.php">Team Overview</a></li>
                </ul>
            </li>
              <li>
                
                <a class="collapsed" href="#sidebar-download" data-toggle="collapse" data-parent="#sidebar">
                    <span class="icon">
                        <i class="fa fa-desktop"></i>
                    </span>
                    Download File
                    <i class="toggle fa fa-angle-down"></i>
                </a>
                <ul id="sidebar-download" class="collapse">
                    <li><a href="download-file.php">Add New File</a></li>
                    
                </ul>
            </li>
              <li>
                <a href="i-trade.php">
                    <span class="icon">
                        <i class="glyphicon glyphicon-th"></i>
                    </span>
                    I-Trade
                </a>
            </li>
             
              <li>
                
                <a class="collapsed" href="#sidebar-about" data-toggle="collapse" data-parent="#sidebar">
                    <span class="icon">
                        <i class="fa fa-desktop"></i>
                    </span>
                   About Us
                    <i class="toggle fa fa-angle-down"></i>
                </a>
                <ul id="sidebar-about" class="collapse">
                    <li><a href="about-us.php">About us</a></li>
                    <li><a href="about-features.php">About Features</a></li>
                     
                </ul>
            </li>
            
              <li>
                
                <a class="collapsed" href="#sidebar-toplink" data-toggle="collapse" data-parent="#sidebar">
                    <span class="icon">
                        <i class="fa fa-desktop"></i>
                    </span>
                  Header Icon Links
                    <i class="toggle fa fa-angle-down"></i>
                </a>
                <ul id="sidebar-toplink" class="collapse">
                    <li><a href="toplink.php">Add Icon Link</a></li>
               </ul>
            </li>
             <li>
                
                <a class="collapsed" href="#sidebar-Menus" data-toggle="collapse" data-parent="#sidebar">
                    <span class="icon">
                        <i class="fa fa-desktop"></i>
                    </span>
                  Menus
                    <i class="toggle fa fa-angle-down"></i>
                </a>
                <ul id="sidebar-Menus" class="collapse">
                    <li><a href="menu.php">View Menus</a></li>
               </ul>
            </li>
              <li>
                
                <a class="collapsed" href="#sidebar-profile" data-toggle="collapse" data-parent="#sidebar">
                    <span class="icon">
                        <i class="fa fa-desktop"></i>
                    </span>
                  Profile
                    <i class="toggle fa fa-angle-down"></i>
                </a>
                <ul id="sidebar-profile" class="collapse">
                    <li><a href="profile-info.php">Edit Profile </a></li>
               </ul>
            </li>
             <li>
                
                <a class="collapsed" href="#sidebar-research" data-toggle="collapse" data-parent="#sidebar">
                    <span class="icon">
                        <i class="fa fa-desktop"></i>
                    </span>
                  Research
                    <i class="toggle fa fa-angle-down"></i>
                </a>
                <ul id="sidebar-research" class="collapse">
                    <li><a href="research.php">Edit Research </a></li>
               </ul>
            </li>
        </ul>
        <!-- every .sidebar-nav may have a title -->
           
         <!-- some styled links in sidebar. ready to use as links to email folders, projects, groups, etc -->
         
          <!-- A place for sidebar notifications & alerts -->
         
    </div>
</nav>
<style>
.widget header  h5{color: #dd5826 !important;font-weight: 700;}
 
</style>