<?php include_once 'db_connection.php'; error_reporting(0); error_reporting(0);
 ?>
<!DOCTYPE html>
<html>
<head>
    <title>BankAsia</title>
    <link href="css/application.min.css" rel="stylesheet">
    <!-- as of IE9 cannot parse css files with more that 4K classes separating in two files -->
    <!--[if IE 9]>
        <link href="css/application-ie9-part2.css" rel="stylesheet">
    <![endif]-->
    <link rel="shortcut icon" href="img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   
</head>
<body class="login-page" style="background-color: #035da9 !important;">

<div class="container">
    <main id="content" class="widget-login-container" role="main">
        <div class="row">
            <div class="col-lg-4 col-sm-6 col-xs-10 col-lg-offset-4 col-sm-offset-3 col-xs-offset-1">
                <h4 class="widget-login-logo animated fadeInUp">
                   
                    <img src="../admin/img/logo-asia.png" alt="LOGO" style="max-width: 100px;" />
                </h4>
                <section class="widget widget-login animated fadeInUp">
                    <header>
                        <h3>Login to your Account</h3>
                          
                    </header>
                    <div class="widget-body">
                        <p class="widget-login-info" style="height: 20px;">
                             <?php  if(isset($_SESSION['msg'])){?>
       <div class="alert alert-warning" role="alert"> <?php echo $_SESSION['msg'] ;?></div>
       <?php } ?>
                        </p>
                        
                        <form method="POST" action="loginaction.php" class="login-form mt-lg">
                         <div class="form-group">
                         
                        <input  name="user_email" id="username-email" placeholder="E-mail" type="email"  class="form-control" required="" />
                    </div>
                    <div class="form-group">
                        
                        <input id="user_password" name="user_password" placeholder="Password" type="password" class="form-control" required=""  />
                    </div>
                    <div class="form-group text-right">
                       
                        <input type="submit" name="Login" class="btn btn-inverse btn-sm" value="Login" />
                    </div>
                              
                           
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <footer class="page-footer" style="color: #fff;">
        2016 &copy; Bankasia All Rights Reserved
    </footer>
</div>
<!-- The Loader. Is shown when pjax happens -->
<div class="loader-wrap hiding hide">
    <i class="fa fa-circle-o-notch fa-spin-fast"></i>
</div>
<style>
.widget-login-container{margin-top: -39px !important;}
.widget-login h3{
      font-size: 17px;
    font-weight: 300;
    text-transform: uppercase;
}
</style>
<!-- common libraries. required for every page-->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/jquery-pjax/jquery.pjax.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/transition.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/collapse.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/button.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/alert.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/widgster/widgster.js"></script>

<!-- common app js -->
<script src="js/settings.js"></script>
<script src="js/app.js"></script>

<!-- page specific libs -->
<!-- page specific js -->
</body>
</html>