<?php include_once 'db_connection.php'; error_reporting(0);
session_start();
if($_SESSION['logged_in'] != true){
header("location:login.php");
}
    $message ="";
     extract($_REQUEST);
    if(isset($_POST['branch_update'])){
  
       $sql="UPDATE branch SET branch_name='$branch_name',branch_email='$branch_email',branch_address='$branch_address',branch_phone='$branch_phone',branch_fax='$branch_fax',incharge_name='$incharge_name',incharge_email='$incharge_email' WHERE branch_id='$branch_id'";
       mysql_query($sql);
        
        $message='<div class="alert alert-success">
        <a class="close" href="#" data-dismiss="alert">
        <i class="fa fa-times-circle"></i>
        </a>
        You  have  Successfully Updated Information.
        </div>';
       
      
 }

 ?>
<!DOCTYPE html>
<html>
<head>
    <title>BankAsia</title>
    <link href="css/application.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
</head>
<body>
 
<?php 
 


include_once 'inc/sidebar.php'; 
include_once 'inc/header.php'; 
?>
<!-- This is the white navigation bar seen on the top. A bit enhanced BS navbar. See .page-controls in _base.scss. -->


 

<div class="content-wrap">
    <!-- main page content. the place to put widgets in. usually consists of .row > .col-md-* > .widget.  -->
    <main id="content" class="content" role="main">
        <ol class="breadcrumb">
            <li>YOU ARE HERE</li>
            <li class="active">Brances</li>
        </ol>
       
       <span class="clearfix"></span>
                        <?php  echo $message ; ?>
         
        <div class="row">
            <div class="col-md-12">
                <section class="widget">
                    <header>
                        <h5>
                            
                            Add New Branch
                        </h5>
                        <div class="widget-controls">
                            
                            <a href="#"><i class="fa fa-refresh"></i></a>
                           
                        </div>
                       
                    </header>
                    <div class="widget-body">
                      <?php 
            $sql="SELECT * FROM branch where branch_id =". $_GET['branch_id'] ; 
            $result= mysql_query($sql);
            while($row=mysql_fetch_assoc($result)){
            ?>
                        <form class="form-horizontal" method="POST" action=" " >
                            <fieldset>
                                <legend> &nbsp;</legend>
                                <input type="hidden" value="<?php echo $row['branch_id']; ?>" name="branch_id" />
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label" for="branch_name">
                                        Branch Name
                                        
                                    </label>
                                    <div class="col-sm-9">
                                       <input type="text" name="branch_name" value="<?php echo $row['branch_name']; ?>" class="form-control" required="" />
                                        
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label" for="branch_email">
                                        Branch Email
                                        
                                    </label>
                                    <div class="col-sm-9">
                                       <input type="email" name="branch_email" class="form-control" value="<?php echo $row['branch_email']; ?>"   />
                                        
                                    </div>
                                </div>
                              <div class="form-group">
                                    <label class="col-sm-3 control-label" for="branch_address">
                                        Branch Address
                                        
                                    </label>
                                    <div class="col-sm-9">
                                       <input type="text" name="branch_address" class="form-control"  value="<?php echo $row['branch_address']; ?>" required="" />
                                         
                                    </div>
                                </div>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label" for="branch_phone">
                                        Branch Phone
                                        
                                    </label>
                                    <div class="col-sm-9">
                                       <input type="text" name="branch_phone" class="form-control" required="" value="<?php echo $row['branch_phone']; ?>" />
                                        
                                    </div>
                                </div>
                              <div class="form-group">
                                    <label class="col-sm-3 control-label" for="branch_fax">
                                        Branch Fax
                                        
                                    </label>
                                    <div class="col-sm-9">
                                       <input type="text" name="branch_fax" value="<?php echo $row['branch_fax']; ?>" class="form-control"   />
                                        
                                    </div>
                                </div>
                                       <div class="form-group">
                                    <label class="col-sm-3 control-label" for="incharge_name">
                                        Incharge Name (optional)
                                        
                                    </label>
                                    <div class="col-sm-9">
                                       <input type="text" name="incharge_name" value="<?php echo $row['incharge_name']; ?>" class="form-control"   />
                                        
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label" for="incharge_email">
                                        Incharge Email (optional)
                                        
                                    </label>
                                    <div class="col-sm-9">
                                       <input type="text" name="incharge_email" class="form-control" value="<?php echo $row['incharge_email']; ?>"  />
                                        
                                          <br /> 
                                    <div class="btn-toolbar">
                                            
                                            <button type="submit" name="branch_update" class="btn btn-md btn-danger pull-right">Save</button>
                                        </div>
                                    </div>
                                    
                                </div>
                            </fieldset>
                        </form>
                        <?php } ?>
                    </div>
                </section>
            </div>
             
        </div>
         <div class="row">
          <?php 
            
            $sql="SELECT * FROM branch";
            $result= mysql_query($sql);
            while($row=mysql_fetch_assoc($result)){
            ?>
                       
                       
        <div class="col-md-4" style="overflow: hidden;">
                <section class="widget">
                    <header>
                        <h5>
                            <?php  echo $row['branch_name']; ?>
                        </h5>
                        <div class="widget-controls">
                               <a href="edit-branch.php?branch_id=<?php echo $row['branch_id'];  ?>" ><i class="glyphicon   glyphicon-edit"></i></a>
                  
                            <a href="del-branch.php?branch_id=<?php echo $row['branch_id'];  ?>" ><i class="glyphicon   glyphicon-trash"></i></a>
                        </div>
                    </header>
                    <div class="widget-body">
                   
                       <p class="text-justify"><?php  echo $row['branch_email'];?></p>
                        <p class="text-justify"><?php  echo $row['branch_phone'];?></p>
                         <p class="text-justify"><?php  echo $row['branch_address'];?></p>
                          <p class="text-justify"><?php  echo $row['branch_fax'];?></p>
                           <p class="text-justify"><?php  echo $row['incharge_name'];?></p>
                          <p class="text-justify"><?php  echo $row['incharge_email'];?></p>
                    </div>
                </section>
            </div>
        
  <?php }?>
        
             
        </div>
         
         
         
         
    </main>
</div>
<!-- The Loader. Is shown when pjax happens -->
<div class="loader-wrap hiding hide">
    <i class="fa fa-circle-o-notch fa-spin-fast"></i>
</div>

<!-- common libraries. required for every page-->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/jquery-pjax/jquery.pjax.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/transition.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/collapse.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/button.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/alert.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/widgster/widgster.js"></script>
<script src="vendor/pace.js/pace.min.js"></script>
<script src="vendor/jquery-touchswipe/jquery.touchSwipe.js"></script>

<!-- common app js -->
<script src="js/settings.js"></script>
<script src="js/app.js"></script>

<!-- page specific libs -->
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js"></script>
<script src="vendor/bootstrap-sass/assets/javascripts/bootstrap/modal.js"></script>
<script src="vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="vendor/jquery-autosize/jquery.autosize.min.js"></script>
<script src="vendor/bootstrap3-wysihtml5/lib/js/wysihtml5-0.3.0.min.js"></script>
<script src="vendor/bootstrap3-wysihtml5/src/bootstrap3-wysihtml5.js"></script>
<script src="vendor/select2/select2.min.js"></script>
<script src="vendor/switchery/dist/switchery.min.js"></script>
<script src="vendor/moment/min/moment.min.js"></script>
<script src="vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script src="vendor/jasny-bootstrap/js/inputmask.js"></script>
<script src="vendor/jasny-bootstrap/js/fileinput.js"></script>
<script src="vendor/holderjs/holder.js"></script>
<script src="vendor/dropzone/dist/min/dropzone.min.js"></script>
<script src="vendor/markdown/lib/markdown.js"></script>
<script src="vendor/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script src="vendor/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js"></script>

<!-- page specific js -->
<script src="js/form-elements.js"></script>
</body>
</html>