-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 17, 2017 at 11:58 AM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shahinsi_bankasia`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutus`
--

CREATE TABLE IF NOT EXISTS `aboutus` (
  `aboutus_id` int(11) NOT NULL,
  `aboutus_detail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aboutus`
--

INSERT INTO `aboutus` (`aboutus_id`, `aboutus_detail`) VALUES
(1, ' <p><span class="dropcap dropcap-bg bg-color-dark">B</span>  Asia is  one  of  the  biggest bank for  BO services. harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>\r\n<br>\r\n <ul>\r\n  <li>Wide Branch Network</li>\r\n  <li>CDBL Services</li>\r\n  </ul>\r\n <p>Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `about_feature`
--

CREATE TABLE IF NOT EXISTS `about_feature` (
  `feature_id` int(11) NOT NULL,
  `feature_title` varchar(1000) NOT NULL,
  `feature_description` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_feature`
--

INSERT INTO `about_feature` (`feature_id`, `feature_title`, `feature_description`) VALUES
(5, 'OUR CORE VALUES:', ''),
(7, 'Customer', '<div>Bank Asia Securities is providing best priority on customer demand through endless effort to assure best satisfaction and sustainable growth in investment.</div>'),
(9, 'Team Member', 'Expert and Experienced team members are ensuring financial growth of clients and companies by providing best and dedicated services.'),
(10, 'Compliance', 'A seasoned compliance team is ensuring best best facilities for our valued customers to rules and regulations.');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `branch_id` int(11) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `branch_email` text NOT NULL,
  `branch_address` varchar(100) NOT NULL,
  `branch_phone` varchar(100) NOT NULL,
  `branch_fax` varchar(100) NOT NULL,
  `incharge_name` varchar(100) DEFAULT NULL,
  `incharge_email` text
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`branch_id`, `branch_name`, `branch_email`, `branch_address`, `branch_phone`, `branch_fax`, `incharge_name`, `incharge_email`) VALUES
(3, 'Head Office', 'info@basl-bd.com', 'Hadi Mansion (7th Floor) 2, Dilkusha<br> Commercial Area Dhaka-1000, Bangladesh', '+88-02-9515826-28', '+88-02-9567884', '', ''),
(4, 'Modhumita Extension Office', 'basl.modhumita@gmail.com', '158-160 Modhumita Building (1st Floor) <br> Motijheel C/A, Dhaka-1000', '+88-01819118893', ' ', 'Md. Kamruzzaman', 'kamruzzaman@basl-bd.com'),
(6, 'Jurain Branch', 'basl.jurain@gmail.com', 'Anaz Tower (2nd Floor) 495, East Jurain Kadamtali, Dhaka-1204<br>', '+88-02-7453617', '', 'Mohammad Asraf Sarkar', 'asraf@basl-bd.com'),
(7, 'Uttara Branch', 'basl.uttara@gmail.com', ' House # 79/A, (4th Floor), Road # 07, Sector # 04 Uttara Model Town, Dhaka-1230', '+88-02-8958371', '', 'Md. Fazle Amin', 'fazleamin@basl-bd.com '),
(8, 'Dhanmondi Branch', 'basl.dhanmondi@gmail.com', ' Meher Plaza (1st Floor), House # 13/A, Road # 05 Dhanmondi, Dhaka - 1207', '+8802-8624874-5', '', 'Md. Abu Taher', 'taher@basl-bd.com '),
(9, 'Khulna Branch', 'basl.khulna@gmail.com', '28, Sir lqbal Road (1st Floor) Khulna-9100<br>', '+88-041-731208-9', '', 'Md. Aminul Islam', 'aminul@basl-bd.com'),
(10, 'Mirpur Branch', 'basl.mirpurbr@gmail.com', 'Nishi Plaza, plot # 01, Avenue-04, Section-06, Block-C Mirpur, Dhaka - 1216<br>', '+88-02-9013841', '', 'Mostofa Nahid Soroar', 'soroar@basl-bd.com');

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE IF NOT EXISTS `career` (
  `career_id` int(2) NOT NULL,
  `career_detail` varchar(1000) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `career`
--

INSERT INTO `career` (`career_id`, `career_detail`) VALUES
(1, 'Asia is  one  of  the  biggest bank for  BO services. harum quidem \r\nrerum facilis est et expedita distinctio lorem ipsum dolor sit amet \r\nconsectetur adipiscing elit. Ut non libero consectetur adipiscing elit \r\nmagna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.\r\n<br>\r\n <ul><li>Wide Branch Network</li><li>CDBL Services</li></ul>\r\n <p>Et harum quidem rerum facilis est et expedita distinctio lorem ipsum\r\n dolor sit amet consectetur adipiscing elit. Ut non libero consectetur \r\nadipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend \r\nenim a feugiat.</p><br>');

-- --------------------------------------------------------

--
-- Table structure for table `download_file`
--

CREATE TABLE IF NOT EXISTS `download_file` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(50) NOT NULL,
  `pdf_file` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `download_file`
--

INSERT INTO `download_file` (`file_id`, `file_name`, `pdf_file`) VALUES
(6, 'BO Account Closing From', '06ba199149e3d336a31f971933ca4d23.pdf'),
(7, 'Online trading manual for investors', '89503e1c00770590a491e3a75b0303aa.pdf'),
(8, 'small affected investor Application form', 'f3f402893428b9185013f12f79132931.pdf'),
(9, 'Sale Order', 'da2873e508b3e140a337cf61a9cad827.pdf'),
(10, 'Account opening requirments', 'c3c384757376a6a9627d9f9ae3f6f929.pdf'),
(11, 'Buy Order', '6e2c50a4f42705d78401a85afabe5b7f.pdf'),
(12, 'Fund Withdrawal Requisitionl', 'd6d5f617def81b488182939f322999bc.pdf'),
(14, 'Account Opening Form', 'eea2d4aaee4b89301b17f94c96c325b7.pdf'),
(15, 'Internet Trading Application form', '2da8f11dd227596a0084b5702785a744.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `footer_link`
--

CREATE TABLE IF NOT EXISTS `footer_link` (
  `link_id` int(11) NOT NULL,
  `link_name` varchar(100) NOT NULL,
  `link_url` text NOT NULL,
  `show_position` int(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer_link`
--

INSERT INTO `footer_link` (`link_id`, `link_name`, `link_url`, `show_position`) VALUES
(13, 'Dhaka Stock Exchange Ltd (DSE)', 'http://dsebd.org/', 1),
(14, 'Chittagong Stock Exchange Ltd (CSE)', 'http://www.cse.com.bd/', 1),
(17, 'Bank Asia Limited', 'www.bankasia-bd.com/home/index', 2),
(18, 'Bangladesh Bank (BB)', 'https://www.bb.org.bd/', 2),
(19, 'Board of Investment (BOI)', 'http://www.boi.gov.bd/', 2),
(20, 'BA Exchange Company (UK) Limited', 'http://www.baexchange.co.uk/', 2),
(21, 'Central Depository Bangladesh Ltd(CDBL) ', 'http://www.cdbl.com.bd/', 1);

-- --------------------------------------------------------

--
-- Table structure for table `itrade`
--

CREATE TABLE IF NOT EXISTS `itrade` (
  `trade_id` int(11) NOT NULL,
  `trade_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itrade`
--

INSERT INTO `itrade` (`trade_id`, `trade_description`) VALUES
(1, '<p> For investors who are already trading through Bank Asia Securities,but not registered for online trade:</p>\r\n<ul>\r\n<li><i></i> Please download the Internet Trading Application form.</li>\r\n<li><i></i>  Fill it and submit to your nearest branch. You will be informed your password and username for Internet Trading Syatem.\r\n</li>\r\n<li><i></i> Confirm the Memorandum of Understanding (MOU) specifying Terms and Conditions for online trading.</li>\r\n<li><i></i> After getting ID &amp; Password, investors have to go to the website for Online Trading Button</li>\r\n<li><i></i>    After first log in, user must change password for future use.</li>\r\n<li><i></i>    For password retrieval (if password forgotten or lost), Please make a request email to info@basl-bd.com.  </li>\r\n<li><i></i>   Users can use this service using any web browser (Internet Explorer)  </li>\r\n<li><i></i>   Users can access his/her Online Trading account from anywhere of the globe, anytime  </li>\r\n</ul>                    \r\n<blockquote>\r\n<p>   Please be informed you have to bear all the risk associated to trading in your account from your user id. If you lose your id and password or any one steal your id and password Bank Asia Securities Limited will not be liable for any damage or unwanted trade in your account. </p>\r\n</blockquote>');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `member_id` int(11) NOT NULL,
  `member_name` varchar(60) NOT NULL,
  `member_designation` varchar(50) NOT NULL,
  `member_message` varchar(10000) NOT NULL,
  `member_image` varchar(100) NOT NULL,
  `chairmen` varchar(11) NOT NULL,
  `key_exe` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `member_name`, `member_designation`, `member_message`, `member_image`, `chairmen`, `key_exe`) VALUES
(6, 'Mr. A. Rouf Chowdhury', 'Chairman', '  text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '97ae44e492790413b50e7ce1b05a4696.jpg', 'yes', ''),
(9, 'Mr. Mohammed Irfan Syed', 'Director', '', '9c52554beab97d568ba98610a27a7682.jpg', '', ''),
(10, 'Ms. Romana Rouf Chowdhury', 'Director', 'adddxcx', '4dda48da986fbc0aa17e96c378ac9db4.jpg', '', ''),
(11, 'Mr. Shah Md. Nurul Alam', 'Director', '', '75bb5eb37dd2e1d22145a4aa149f2503.jpg', '', ''),
(12, 'Mr.Md. Mehmood Husain', 'Mr.Md. Mehmood Husain', '', '1f639bb6775c85fadd83813d2ffd77ef.jpg', '', ''),
(13, 'Mr. Aminul Islam', 'Director', '', '5bd9f7e47584d42b2b4d3099e5ef3115.jpg', '', ''),
(14, 'Mr. Md. Arfan Ali', 'Director', '', '1d284fdcbcf80cb0225301e0c1c46841.jpg', '', ''),
(15, 'Mr. Imran Ahmed', 'Director', '', '5fc998c2da08e619c4e55f56226da507.jpg', '', ''),
(21, 'Mr. Sumon Das ', 'VP & CEO', '', '7dc02523bd51b449a2f75a253856b93c.png', '', 'yes'),
(22, 'Md. Anisul Alam Sarker ', 'FAVP & Head of Operation', '', '3951cd1868d395c7533efe4480d1d369.png', '', 'yes'),
(25, ' Mr. Anisur Rahman Sinha ', 'Director', 'dfdfdfdgfd', '1fbd489061f86c2302e9be49baa1002e.jpg', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `menu_id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `name`) VALUES
(1, 'Home'),
(2, 'About'),
(3, 'About BASL'),
(4, 'Profile'),
(5, 'Board Of Director''s'),
(6, 'Management'),
(7, 'Services'),
(8, 'Research'),
(9, 'Publication'),
(10, 'Internet Trading'),
(11, 'Contact  Us'),
(12, 'Download'),
(13, 'News & Events'),
(14, 'Career');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(55) NOT NULL,
  `news_name` varchar(255) NOT NULL,
  `news_description` varchar(1000) NOT NULL,
  `news_date` varchar(250) NOT NULL,
  `post_image` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `news_name`, `news_description`, `news_date`, `post_image`) VALUES
(3, 'this  is  our second seminar', '<span>Customer Benefit\r\n                                                    <br>\r\n                                                    <span>\r\n                                                    	<span>\r\n                                                        	<span>\r\n                                                                <span><span>\r\n                                                                	<img src="http://www.bankasia-bd.com/images/square1.gif" alt="">\r\n                                                                	Cheque-book facility\r\n                                                                </span>\r\n                                                    			<br>\r\n                                                                <span>\r\n                                                                	<img src="http://www.bankasia-bd.com/images/square1.gif" alt="">\r\n                                                                	Opportunity to apply for - safe de', '10  th  jan 2016', '87c460c5e86b0787e06a10ee5753651e.jpg'),
(2, 'this  is  our first seminar', 'Bank Asia is  one  of  the  biggest bank for  BO services. harum quidem \r\nrerum facilis est et expedita distinctio lorem ipsum dolor sit amet \r\nconsectetur adipiscing elit. Ut non libero consectetur adipiscing elit \r\nmagna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.nk Asia is  one  of  the  biggest bank for  BO services. harum quidem \r\nrerum facilis est et expedita distinctio lorem ipsum dolor sit amet \r\nconsectetur adipiscing elit. Ut non libero consectetur adipiscing elit \r\nmagna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.nk Asia is  one  of  the  biggest bank for  BO services. harum quidem \r\nrerum facilis est et expedita distinctio lorem ipsum dolor sit amet \r\nconsectetur adipiscing elit. Ut non libero consectetur adipiscing elit \r\nmagna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.nk Asia is  one  of  the  biggest bank for  BO services. harum quidem \r\nrerum facilis est et expedita distinctio lorem ipsum dolor sit amet \r\nconsec', '10  th  jan 2016', '8f7b579a77f5c4f597347dc306839edb.png'),
(4, 'this  is  our third seminar', '<span>Customer Benefit\r\n                                                    <br>\r\n                                                    <span>\r\n                                                    	<span>\r\n                                                        	<span>\r\n                                                                <span><span>\r\n                                                                	<img src="http://www.bankasia-bd.com/images/square1.gif" alt="">\r\n                                                                	Cheque-book facility\r\n                                                                </span>\r\n                                                    			<br>\r\n                                                                <span>\r\n                                                                	<img src="http://www.bankasia-bd.com/images/square1.gif" alt="">\r\n                                                                	Opportunity to apply for - safe de', '10  th  jan 2014', '09d2eb49316c8b9684b1205ca1221c05.png'),
(5, 'this is our second seminar', 'this is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminarthis is our second seminar', '10  th  jan 2014', '65a00adbec812f1c66794d0a27683d3a.jpg'),
(6, 'this is our second seminar', '<h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><h5>this  is  our second seminar</h5><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>', '10  th  Feb 2016', '8381056bf252a1779d31a9fc8d73d4c6.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(20) NOT NULL,
  `profile_info` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `profile_info`) VALUES
(1, '<br>\r\n                 \r\n                Welcome to Bank Asia \r\nSecurities Limited (BASL) is a Majority owned subsidiary company of Bank\r\n Asia Limited has been operating as Stock Broker &amp; Stock Dealer \r\nactivities from April 17, 2011. Before subsidiary, it was a division of \r\nBank Asia since 5th August, 2009. BASL is playing leading position on \r\ndaily turnover and providing the best services to the clients for the \r\nsustainable growth of the Capital Market.<br><br><br>\r\n                 \r\n                Welcome to Bank Asia \r\nSecurities Limited (BASL) is a Majority owned subsidiary company of Bank\r\n Asia Limited has been operating as Stock Broker &amp; Stock Dealer \r\nactivities from April 17, 2011. Before subsidiary, it was a division of \r\nBank Asia since 5th August, 2009. BASL is playing leading position on \r\ndaily turnover and providing the best services to the clients for the \r\nsustainable growth of the Capital Market. <br><br><br>');

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE IF NOT EXISTS `publication` (
  `id` int(20) NOT NULL,
  `publication_info` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publication`
--

INSERT INTO `publication` (`id`, `publication_info`) VALUES
(1, 'Welcome to Bank Asia \r\nSecurities Limited (BASL) is a Majority owned subsidiary company of Bank\r\n Asia Limited has been operating as Stock Broker &amp; Stock Dealer \r\nactivities from April 17, 2011. Before subsidiary, it was a division of \r\nBank Asia since 5th August, 2009. BASL is playing leading position on \r\ndaily turnover and providing the best services to the clients for the \r\nsustainable growth of the Capital Market.<br><br>Welcome to Bank Asia \r\nSecurities Limited (BASL) is a Majority owned subsidiary company of Bank\r\n Asia Limited has been operating as Stock Broker &amp; Stock Dealer \r\nactivities from April 17, 2011. Before subsidiary, it was a division of \r\nBank Asia since 5th August, 2009. BASL is playing leading position on \r\ndaily turnover and providing the best services to the clients for the \r\nsustainable growth of the Capital Market.<br><br>Welcome to Bank Asia \r\nSecurities Limited (BASL) is a Majority owned subsidiary company of Bank\r\n Asia Limited has been operating as Stock Broker &amp; Stock Dealer \r\nactivities from April 17, 2011. Before subsidiary, it was a division of \r\nBank Asia since 5th August, 2009. BASL is playing leading position on \r\ndaily turnover and providing the best services to the clients for the \r\nsustainable growth of the Capital Market.<br><br>Welcome to Bank Asia \r\nSecurities Limited (BASL) is a Majority owned subsidiary company of Bank\r\n Asia Limited has been operating as Stock Broker &amp; Stock Dealer \r\nactivities from April 17, 2011. Before subsidiary, it was a division of \r\nBank Asia since 5th August, 2009. BASL is playing leading position on \r\ndaily turnover and providing the best services to the clients for the \r\nsustainable growth of the Capital Market.<br><br><br><br>');

-- --------------------------------------------------------

--
-- Table structure for table `research`
--

CREATE TABLE IF NOT EXISTS `research` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `daily` varchar(20) NOT NULL,
  `monthly` varchar(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `research`
--

INSERT INTO `research` (`id`, `name`, `date`, `file_name`, `daily`, `monthly`) VALUES
(12, 'Research  Two', '2015-11-04', '8576172651c841132c04e26ac9009e9b.pdf', 'yes', ''),
(15, 'Research  three', '2015-11-04', 'f97e8e114f6f031cdb07723a2bad89ac.pdf', '', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE IF NOT EXISTS `service` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(200) NOT NULL,
  `service_description` text NOT NULL,
  `service_image` varchar(40) DEFAULT NULL,
  `show_position` int(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`service_id`, `service_name`, `service_description`, `service_image`, `show_position`) VALUES
(54, 'Panel Broking ', 'is a long established fact that a reader will be distracted by the \r\nreadable content of a page when looking at its layout. The point of \r\nusing Lorem Ipsum is that it has a more-or-less normal distribution of \r\nletters, as opposed to using ''Content here, content here'', making it \r\nlook like readable English. Many desktop publishing packages and web \r\npage editors now use Lorem Ipsum as their default model text, and a \r\nsearch for ''lorem ipsum'' will uncover many web sites still in their \r\ninfancy. Various versions have evolved over the years, sometimes by \r\naccident, sometimes on purpose (injected humour and the like).<br>', '1e7d8e1ca769ef96045aa144f498f339.png', 1),
(55, 'Margin Loan', 'is A a long established fact that a reader will be distracted by the \r\nreadable content of a page when looking at its layout. The point of \r\nusing Lorem Ipsum is that it has a more-or-less normal distribution of \r\nletters, as opposed to using ''Content here, content here'', making it \r\nlook like readable English. Many desktop publishing packages and web \r\npage editors now use Lorem Ipsum as their default model text, and a \r\nsearch for ''lorem ipsum'' will uncover many web sites still in their \r\ninfancy. Various versions have evolved over the years, sometimes by \r\naccident, sometimes on purpose (injected humour and the like).<br>', 'eb1203f33f4a2673aed55db708b4c800.jpg', 1),
(56, 'CDBL Services', 'is a long established fact that a reader will be distracted by the \r\nreadable content of a page when looking at its layout. The point of \r\nusing Lorem Ipsum is that it has a more-or-less normal distribution of \r\nletters, as opposed to using ''Content here, content here'', making it \r\nlook like readable English. Many desktop publishing packages and web \r\npage editors now use Lorem Ipsum as their default model text, and a \r\nsearch for ''lorem ipsum'' will uncover many web sites still in their \r\ninfancy. Various versions have evolved over the years, sometimes by \r\naccident, sometimes on purpose (injected humour and the like).<br>', '483913a8bff4d4eec4094b35e94642d7.jpg', 1),
(57, 'Brokerage Services ', '<p><span>We are\r\nplease to inform you that the management of Bank Asia Securities Limited is\r\ngoing to launch a new product named â€˜<b>Absolute\r\nCash</b>. The features of this new product are:</span></p>&nbsp;\r\n<br>Transaction\r\nCommission&nbsp;<b>&nbsp; </b><b>0.25%</b><br>Account\r\nOpening Charge&nbsp; <b>1,000/-</b><br>Retention\r\nAmount&nbsp; <b>500/-</b><span><br>Margin\r\nFacility&nbsp; </span><b>Not\r\nAllowed <br></b>Migration\r\nFacility&nbsp; <b>Not\r\nAllowed<br></b>Buy Against Immature Balance&nbsp; <b>Not\r\nAllowed<br><br></b><br><p><b>Panel Broker with Merchant Banks:</b></p>\r\n\r\n<ul><li>Jamuna Bank Capital\r\n     Management Limited</li><li>IL Capital Limited</li><li>IDLC Investments Limited</li></ul><br>', '6a7427dbd4fd1658a64eea3176da027d.jpg', 1),
(58, 'Research', '<p><span>We are\r\nplease to inform you that the management of Bank Asia Securities Limited is\r\ngoing to launch a new product named â€˜<b>Absolute\r\nCash</b>. The features of this new product are:</span></p>&nbsp;\r\n<br>Transaction\r\nCommission&nbsp;<b>&nbsp; </b><b>0.25%</b><br>Account\r\nOpening Charge&nbsp; <b>1,000/-</b><br>Retention\r\nAmount&nbsp; <b>500/-</b><span><br>Margin\r\nFacility&nbsp; </span><b>Not\r\nAllowed <br></b>Migration\r\nFacility&nbsp; <b>Not\r\nAllowed<br></b>Buy Against Immature Balance&nbsp; <b>Not\r\nAllowed</b><br><br>\r\n\r\n<p><b>Panel Broker with Merchant Banks:</b></p>\r\n\r\n<ul><li>Jamuna Bank Capital\r\n     Management Limited</li><li>IL Capital Limited</li><li>IDLC Investments Limited</li></ul>\r\n\r\n<br><br>', 'f3e21007341cba92acf0deab543d2ecf.jpg', 1),
(61, 'Wide Branch Network', 'is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distri', NULL, 1),
(62, 'Attractive Commission Rate', 'is a long established fact that a reader will be distracted by the \r\nreadable content of a page when looking at its layout. The point of \r\nusing Lorem Ipsum is that it has a more-or-less normal distri<br>', NULL, 1),
(63, 'Portfolio & Risk Management', 'is a long established fact that a reader will be distracted by the \r\nreadable content of a page when looking at its layout. The point of \r\nusing Lorem Ipsum is that it has a more-or-less normal distri<br>', NULL, 1),
(64, 'Institutional & Foreign Trade', 'is a long established fact that a reader will be distracted by the \r\nreadable content of a page when looking at its layout. The point of \r\nusing Lorem Ipsum is that it has a more-or-less normal distri<br>', NULL, 1),
(65, 'Internet & Smartphone Trading', 'is a long established fact that a reader will be distracted by the \r\nreadable content of a page when looking at its layout. The point of \r\nusing Lorem Ipsum is that it has a more-or-less normal distri<br>', NULL, 1),
(66, 'Daily Market Updates & News Summary', 'We are please to inform you that the management of Bank Asia Securities Limited is going to launch a new product named â€˜Absolute Cash. The features of this new product are:', '361fc78ec9f1732df82292619fe5e4d3.jpg', 0),
(69, 'fdsfdsf', 'sdsdsd', '08dcda6ada0774bb352c242f6967e6d6.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `team_overview`
--

CREATE TABLE IF NOT EXISTS `team_overview` (
  `overview_id` int(11) NOT NULL,
  `overview` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team_overview`
--

INSERT INTO `team_overview` (`overview_id`, `overview`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Table structure for table `toplink`
--

CREATE TABLE IF NOT EXISTS `toplink` (
  `id` int(20) NOT NULL,
  `place_title` varchar(255) NOT NULL,
  `file_link` varchar(255) NOT NULL,
  `img_file` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toplink`
--

INSERT INTO `toplink` (`id`, `place_title`, `file_link`, `img_file`) VALUES
(8, 'Bangladesh Securities &  Exchange Commission', 'http://www.sec.gov.bd/', '7f93bc2dac1790419a10752595274c0d.png'),
(7, 'Dhaka Stock Exchange', 'http://www.dsebd.org/', '657fc9b7d82994b84d834259fb84e4db.png'),
(6, 'Facebook Page  ', 'https://www.facebook.com/Bank-Asia-Securities-Ltd-752207171492523/?fref=ts', 'dfa04cb8fbb9705a38fd00bacacf5ac4.png'),
(9, 'Chittagong Stock Exchange', 'http://www.cse.com.bd/', '386d9879ef6288ac14a464bfb770860c.png'),
(10, 'Download Forms', 'http://bankasia.silkyit.com/download.php', '011c30b4fb1de74e7beab9a3c17b53e6.png'),
(12, 'bank Asia', 'http://www.bankasia-bd.com/', 'd5e3ed2ce7f2e3f480541ac8bd4254f6.png'),
(18, 'Central Depository Bangladesh Ltd', 'http://www.cdbl.com.bd/', '4c12284f26a62181ed88083fde2670e8.png'),
(17, 'Bangladesh Bank', 'https://www.bb.org.bd/', 'fe9c020f74dc34b5f867f0a5c839f1ca.png');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `admin_email` varchar(20) NOT NULL,
  `admin_password` varchar(1000) NOT NULL,
  `admin_name` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `admin_email`, `admin_password`, `admin_name`) VALUES
(1, 'monirjss@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'monirul  islam');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aboutus`
--
ALTER TABLE `aboutus`
  ADD PRIMARY KEY (`aboutus_id`);

--
-- Indexes for table `about_feature`
--
ALTER TABLE `about_feature`
  ADD PRIMARY KEY (`feature_id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`career_id`);

--
-- Indexes for table `download_file`
--
ALTER TABLE `download_file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `footer_link`
--
ALTER TABLE `footer_link`
  ADD PRIMARY KEY (`link_id`);

--
-- Indexes for table `itrade`
--
ALTER TABLE `itrade`
  ADD PRIMARY KEY (`trade_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `research`
--
ALTER TABLE `research`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `team_overview`
--
ALTER TABLE `team_overview`
  ADD PRIMARY KEY (`overview_id`);

--
-- Indexes for table `toplink`
--
ALTER TABLE `toplink`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `admin_email` (`admin_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_feature`
--
ALTER TABLE `about_feature`
  MODIFY `feature_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `branch_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `career_id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `download_file`
--
ALTER TABLE `download_file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `footer_link`
--
ALTER TABLE `footer_link`
  MODIFY `link_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(55) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `publication`
--
ALTER TABLE `publication`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `research`
--
ALTER TABLE `research`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `toplink`
--
ALTER TABLE `toplink`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
