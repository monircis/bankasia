<?php error_reporting(0); include 'db_connection.php';  ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Home Page</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-v4.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/flexslider/flexslider.css">     
    <link rel="stylesheet" href="assets/plugins/bxslider/jquery.bxslider.css">
    <link rel="stylesheet" href="assets/plugins/fancybox/source/jquery.fancybox.css"> 
    <link rel="stylesheet" href="assets/plugins/revolution-slider/rs-plugin/css/settings.css" type="text/css" media="screen">
    <!--[if lt IE 9]><link rel="stylesheet" href="assets/plugins/revolution-slider/rs-plugin/css/settings-ie8.css" type="text/css" media="screen"><![endif]-->

    <!-- CSS Pages Style -->    
    <link rel="stylesheet" href="assets/css/pages/page_one.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css"/>
</head>	

<body class="boxed-layout container" style="margin-bottom: 30px;">    

<div class="wrapper" style="border-radius:0 0 27px 27px;">     
    <!--=== Header ===-->    
     <div class="header-v4">
         <div class=" container">
            <!-- Logo -->
             <div class=" col-lg-12 no-padding  ">
            <a class="logo pull-left" href="/">
                <img src="img/0_01.png" class="animated fadeInDown"  style="  border-radius: 2px !important;
    height: 47px;
    margin-top: 31px;" alt="Logo" />
            </a>
             
          <div class="pull-right">
          <ul class="top_lists">
          <li><a href="">Login</a></li>
           <li><a href="">Register</a></li>
          </ul>
          <span class="clearfix"></span>
           <div class="search-open">
                            <div class="input-group animated fadeInDown">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn-u" type="button"><i class="fa  fa-search"></i></button>
                                </span>
                            </div>
                        </div>    
                         <span class="clearfix"></span>
                          <ul class="top_lists">
         
           <li><a href="">Internet Banking</a></li>
           <li>|</li>
            <li><a href="">Forms & Downloads</a></li>
              <li>|</li>
             <li><a href="">Mothly Reports</a></li>
             
          </ul>
         
          	
 
 
          </div>
             
           
                
            
              
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                       <i class="fa fa-bars"></i>
                    </button>
            <!-- End Topbar -->
<br /><br /><br /><br /> 
 
        </div><!--/end container-->
    <span class="clearfix"></span>
        <!-- Navbar -->
        <div class="navbar navbar-default mega-menu" role="navigation">
              

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-responsive-collapse">
                <div class="container no-padding">
                
                    <ul class="nav navbar-nav">
                 
                        <li class="dropdown">
                            <a href="http://bankasia.silkyit.com/"  >
                                <?php  $sql="SELECT *  FROM menu where menu_id=1" ; $result= mysql_query($sql); $row=mysql_fetch_array($result); echo $row['name'];   ?>
                            </a>
                           
                        </li>
                           <li class="divider">|</li>   
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                   <?php  $sql="SELECT  * FROM menu where menu_id=2" ; $result= mysql_query($sql);$row=mysql_fetch_array($result); echo $row['name'];  ?>
                            </a>
                            <ul class="dropdown-menu">
                             <ul class="list-unstyled equal-height-list">
                                                        <li><h3>About</h3></li>
                            	<li>
													<a href="about.php"><?php 
                                            $sql="SELECT * FROM menu where menu_id=3" ; $result= mysql_query($sql); while($row=mysql_fetch_array($result)){ echo $row['name']; }
                                            ?></a>
											</li>
                                            	<li>
														<a href="profile.php"><?php 
                                            $sql="SELECT * FROM menu where menu_id=4" ; $result= mysql_query($sql); while($row=mysql_fetch_array($result)){ echo $row['name']; }
                                            ?></a>
											</li>
                                         	<li>
														<a href="director.php"><?php 
                                            $sql="SELECT * FROM menu where menu_id=5" ; $result= mysql_query($sql); while($row=mysql_fetch_array($result)){ echo $row['name']; }
                                            ?></a>
											</li>
										 	<li>
														<a href="management.php"><?php 
                                            $sql="SELECT * FROM menu where menu_id=6" ; $result= mysql_query($sql); while($row=mysql_fetch_array($result)){ echo $row['name']; }
                                            ?></a>
											</li>
                                                    </ul>
                            </ul>
                        </li>
                           <li class="divider">|</li>   
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                   <?php  $sql="SELECT  * FROM menu where menu_id=7" ; $result= mysql_query($sql); while($row=mysql_fetch_array($result)){ echo $row['name']; }  ?>
                            </a>
                            <ul class="dropdown-menu">
                             <ul class="list-unstyled equal-height-list">
                                                        <li><h3>BASL Services</h3></li>
                                            <?php 
                                            $sql="SELECT * FROM service" ; 
                                            $result= mysql_query($sql);
                                            while($row=mysql_fetch_assoc($result)){ ?>
                                            <li>
                                            <a href="service.php?service_id=<?php echo $row['service_id']; ?>"><?php echo $row['service_name']; ?> </a>
                                            </li>
                                            <?php } ?>
                                                    </ul>
                            </ul>
                        </li>
                          
                          <li class="divider hidden-xs">|</li>      
                           <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                   <?php  $sql="SELECT  * FROM menu where menu_id=8" ; $result= mysql_query($sql); while($row=mysql_fetch_array($result)){ echo $row['name']; }  ?>
                            </a>
                            <ul class="dropdown-menu">
                             <ul class="list-unstyled equal-height-list">
                                                        <li><h3>BASL Research</h3></li>
                                                      	<li>
												<a href="research.php"><?php 
                                            $sql="SELECT * FROM menu where menu_id=8" ; $result= mysql_query($sql); while($row=mysql_fetch_array($result)){ echo $row['name']; }
                                            ?></a>
											</li>
                                        	<li>
												<a href="publications.php"><?php 
                                            $sql="SELECT * FROM menu where menu_id=9" ; $result= mysql_query($sql); while($row=mysql_fetch_array($result)){ echo $row['name']; }
                                            ?></a>
											</li>
                                                    </ul>
                            </ul>
                        </li>
                        <li class="divider hidden-xs">|</li>   
                           <li class="dropdown">
                           	<a href="https://investor.dsetrade.com" target="_blank">
									 <?php 
                                            $sql="SELECT * FROM menu where menu_id=10" ; $result= mysql_query($sql); while($row=mysql_fetch_array($result)){ echo $row['name']; }
                                            ?>
										</a>
                            
                        </li>
                       
                          <li class="divider hidden-xs">|</li>   
                            <li class="dropdown">
                            <a href="news.php" >
                               <?php 
                                            $sql="SELECT * FROM menu where menu_id=13" ; $result= mysql_query($sql); while($row=mysql_fetch_array($result)){ echo $row['name']; }
                                            ?>
                            </a>
                            
                        </li>
                         <li class="divider hidden-xs">|</li> 
                                    	<li>
										<a href="career.php">
										<?php 
                                            $sql="SELECT * FROM menu where menu_id=14" ; $result= mysql_query($sql); while($row=mysql_fetch_array($result)){ echo $row['name']; }
                                            ?>
										</a>
									</li>
                         <li class="divider hidden-xs">|</li> 
                                    	<li>
										<a href="download.php">
										<?php 
                                            $sql="SELECT * FROM menu where menu_id=12" ; $result= mysql_query($sql); while($row=mysql_fetch_array($result)){ echo $row['name']; }
                                            ?>
										</a>
									</li>
                              <li class="divider hidden-xs">|</li>   
                        	<li>
										<a href="contact.php">
									<?php 
                                            $sql="SELECT * FROM menu where menu_id=11" ; $result= mysql_query($sql); while($row=mysql_fetch_array($result)){ echo $row['name']; }
                                            ?>
										</a>
									</li>
                                    
                         
                    </ul>
 
                </div><!--/end container-->
            </div><!--/navbar-collapse-->
        </div>            
        <!-- End Navbar -->
    </div>
    <!--=== End Header ===-->